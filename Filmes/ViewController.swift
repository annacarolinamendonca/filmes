//
//  ViewController.swift
//  Filmes
//
//  Created by Anna Carolina on 07/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var filmes: [Filme] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var filme: Filme
        filme = Filme(titulo:" 007 Contra SPECTRE", descricao: "James Bond chega à Cidade do México e está pronto para eliminar Marco Sciarra sem que o chef M saiba da missão. O caso leva à suspensão temporária do agente, que passa a ser constantemente vigiado pelo governo britânico graças a uma tecnologia implantada em seu corpo por Q. Na tentativa de despistar os inimigos e até mesmo alguns de seus parceiros de trabalho, ele se responsabiliza por ajudar a filha de um desafeto. Toda a situação o leva ao centro de uma temida organização denominada Spectre.",
                      imagem: UIImage(named: "filme1") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Star Wars: Episódio VII – O Despertar da Força", descricao: "A queda de Darth Vader e do Império levou ao surgimento de uma nova força sombria: a Primeira Ordem. Eles procuram o jedi Luke Skywalker, desaparecido. A resistência tenta desesperadamente encontrá-lo antes para salvar a galáxia.",
                      imagem: UIImage(named: "filme2") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Impacto Mortal", descricao: "Colt (Freida Pinto) e John (Ryan Kwanten) são pistoleiros, amantes e companheiros num controverso e obscuro mundo de duelos perigosos, e contam com a ajuda um do outro para encontrar e eliminar o assassino do irmão de Colt, vingando assim sua morte.",
                       imagem: UIImage(named: "filme3") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Deadpool", descricao: "Wade Wilson é um ex-agente especial que passou a trabalhar como mercenário. Seu mundo é destruído quando um cientista maligno o tortura e o desfigura completamente. O experimento brutal transforma Wade em Deadpool, que ganha poderes especiais de cura e uma força aprimorada. Com a ajuda de aliados poderosos e um senso de humor mais desbocado e cínico do que nunca, o irreverente anti-herói usa habilidades e métodos violentos para se vingar do homem que quase acabou com a sua vida.",
                       imagem: UIImage(named: "filme4") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"O Regresso", descricao: "Hugh Glass (Leonardo DiCaprio) parte para o oeste americano disposto a ganhar dinheiro caçando. Atacado por um urso, fica seriamente ferido e é abandonado à própria sorte pelo parceiro John Fitzgerald (Tom Hardy), que ainda rouba seus pertences. Entretanto, mesmo com toda adversidade, Glass consegue sobreviver e inicia uma árdua jornada em busca de vingança.",
                       imagem: UIImage(named: "filme5") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"A Herdeira", descricao: "Dina (Rebecca Emilie Sattrup) herdou da mãe a habilidade sobrenatural de olhar a alma das pessoas, fazendo com que os observados tenham vergonha. Ela é requisitada para fazer com que o herdeiro de um trono confesse ter assassinado sua família, mas se recusa a usar seu dom para fins malignos e acaba presa, o que faz com que ela precise descobrir quem é o verdadeiro culpado dos crimes para salvar a si própria.",
                       imagem: UIImage(named: "filme6") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Caçadores de Emoção", descricao: "Um jovem agente do FBI (Luke Bracey) tem como missão se infiltrar em meio a atletas de esportes radicais, suspeitos de cometerem uma série de roubos nunca vistos até então. Não demora muito para que ele se aproxime de Bodhi (Édgar Ramirez), o líder do grupo, e conquiste sua confiança.",
                       imagem: UIImage(named: "filme7") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Regresso do mal", descricao: "Durante um desfile na noite de Halloween, um menino de oito anos desaparece misteriosamente. Depois de um ano sem qualquer pista, os pais do garoto começam a sentir presenças estranhas. O casal decide se unir para procurar seu filho em toda a cidade de Nova York. O que ninguém esperava é a descoberta de um espirito vingativo, cheio de segredos antigos que colocam a vida do menino em perigo.",
                       imagem: UIImage(named: "filme8") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Tarzan", descricao: "A lenda de Tarzan, na qual um pequeno garoto órfão é criado na selva, e mais tarde tenta se adaptar à vida entre os humanos. Na década de 30, Tarzan, aclimatado à vida em Londres em conjunto com sua esposa Jane, é chamado para retornar à selva onde passou a maior parte da sua vida onde servirá como um emissário do Parlamento Britânico.",
                       imagem: UIImage(named: "filme9") ?? UIImage())
        filmes.append(filme)
        
        filme = Filme (titulo:"Hardcore: Missão Extrema", descricao: "dHenry, um ciborgue recém-ressucitado deve salvar sua esposa/criadora das garras de um tirano psicótico com poderes telecináticos, Akan, e seu exército de mercenários. Lutando ao lado dele, Jimmy é a única esperança de Henry para realizar tal feito até o final do dia. ",
                       imagem: UIImage(named: "filme10") ?? UIImage())
        filmes.append(filme)

    }
    //exibir os filmes na tela
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let filme = filmes [indexPath.row]
        let celulaReuso = "celulaReuso"
        
        let celula = tableView.dequeueReusableCell(withIdentifier: celulaReuso, for: indexPath) as! FilmeCelula
        celula.filmeImagem.image = filme.imagem
        celula.labelTitulo.text = filme.titulo
        celula.labelDescricao.text = filme.descricao
        
        celula.filmeImagem.layer.cornerRadius = 42
        celula.filmeImagem.clipsToBounds = true
    
        /* celula.textLabel?.text = filme.titulo
        celula.imageView?.image = filme.imagem*/
        
        return celula

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detalhesFilme"{
            
            if let indexPath = tableView.indexPathForSelectedRow{
                
                let filmeSelecionado = self.filmes [indexPath.row]
                let viewControllerDestino = segue.destination as! DetalhesFilmeViewController
                viewControllerDestino.filme = filmeSelecionado
                
            }
            
        }
    
    }

}

