//
//  FilmeCelula.swift
//  Filmes
//
//  Created by Anna Carolina on 12/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class FilmeCelula: UITableViewCell{
    
    @IBOutlet weak var filmeImagem: UIImageView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelDescricao: UILabel!
}


