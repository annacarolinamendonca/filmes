//
//  filme.swift
//  Filmes
//
//  Created by Anna Carolina on 07/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit
class Filme{
    
    var titulo: String!
    var descricao: String!
    var imagem: UIImage!
    
    init(titulo: String, descricao: String, imagem: UIImage = UIImage()) {
        self.titulo = titulo
        self.descricao = descricao
        self.imagem = imagem
        
    }
    

}
