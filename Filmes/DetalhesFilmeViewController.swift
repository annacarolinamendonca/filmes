//
//  DetalhesFilmeViewController.swift
//  Filmes
//
//  Created by Anna Carolina on 12/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class DetalhesFilmeViewController: UIViewController {


    @IBOutlet weak var filmeImagemDetalhes: UIImageView!
    
    @IBOutlet weak var tituloDetalhes: UILabel!
    
    @IBOutlet weak var descricaoDetalhes: UILabel!
    
    
    var filme: Filme!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filmeImagemDetalhes.image = filme.imagem
        tituloDetalhes.text = filme.titulo
        descricaoDetalhes.text = filme.descricao
    
    }
    
}
